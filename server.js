const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const messageRouter = require('./router/messages');
const userRoute = require('./router/user');

const app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const cors = require('cors')
app.use(bodyParser.json());

app.use(cors());
app.use('', messageRouter);
app.use('', userRoute);


io.on("connection", socket => {
    // Log whenever a user connects
    console.log(socket.id, "user connected");
    io.emit('connected', {
        status: 'connected',
        id: socket.id
    })

    // Log whenever a client disconnects from our websocket server
    socket.on("disconnect", () => {
        console.log(socket.id, 'Disconnects')
        io.emit("disconnect", {
            status: "disconnected",
            id: socket.id
        })
    });

    socket.on("join", (data) => {
        console.log(data);
        socket.broadcast.emit('join', {
            user: data,
            id: socket.id
        });
    })

    // When we receive a 'message' event from our client, print out
    // the contents of that message and then echo it back to our client
    // using `io.emit()`
    socket.on("message", message => {
        console.log("Message Received: " + message);
        io.emit("message", {
            message: message,
            id: socket.id
        });
    });

    // when user is typing 
    socket.on('isTypping', () => {
        socket.broadcast.emit('isTypping', {
            status: 'typping...',
            id: [socket.id]
        })
        console.log('user is typing')
    })
});

mongoose.connect('mongodb+srv://stupidDB:test@stupidcluster-xxowk.mongodb.net/test?retryWrites=true&w=majority', {
    useNewUrlParser: true
}, (err) => {
    if (err) {
        console.log(err)
    }
    console.log('db connected successfully')
})

var server = http.listen(process.env.PORT || 3020, () => {
    console.log("Well done, now I am listening on ", server.address().port)
})