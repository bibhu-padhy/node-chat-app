const mongoose = require('mongoose')


const messageModel = new mongoose.Schema({
    message: {
        type: String,
    },
    senderId: {
        type: String,
    },
    date: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('messagesCollections', messageModel)