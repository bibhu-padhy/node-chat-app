const route = require('express').Router();
const User = require('../models/user');
const ObjectId = require('mongoose').Types.ObjectId
const bcrypt = require('bcrypt');


const {
    sign
} = require('jsonwebtoken')



route.post('/register', async (req, res) => {
    const check = await User.findOne({
        email: req.body.email
    })
    if (check) {
        // if the user exist 
        res.status(400).send('That user already exisits!')
    } else {
        // if new user
        const hassedPassword = await bcrypt.hash(req.body.password, 6) // here it will hass the password 
        const user = new User({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            password: hassedPassword, // here it will store the hassed password 
        });
        user.save((err, userData) => {
            if (err) {
                res.send(err);
            } else {
                res.send(userData);
            }
        });
    }
});

route.post('/login', async (req, res) => {
    const user = await User.findOne({
        email: req.body.email,
    });
    if (user) { // if the given email matches it will go to this block
        const valid = bcrypt.compare(req.body.password, user.password) // then it will compare the passwords
        valid.then((data) => {
            if (data) { // if the given password matches 
                const token = sign({ // it will create a jwt token 
                    id: user._id,
                    user: user.email
                }, 'QWuighvbmn')
                res.status(200).send({ // then here it will send the token 
                    access_token: token,
                    user: user.email,
                    userName: user.firstname + ' ' + user.lastname,
                    id: user._id
                });
            } else { // if users password is invalid 
                res.status(400).send('user name and password is not matching')
            }
        })
    } else { // if there is no user by this given email 
        res.status(400).send('User does not exist')
    }
})

// get users list
route.get('/userslist', (req, res) => {

    User.find({},'-password',(err, msgData) => {
        if (!err) {
            res.status(200).send(msgData);
        }

    })
})

// get one user
route.get('/:id', (req, res) => {

    if (!ObjectId.isValid(req.params.id)) {
        res.send('not a valid id')
    } else {
        User.findById(req.params.id, (err, doc) => {
            if (!err) {
                res.send(doc);
            }
        })
    }
})




module.exports = route