const express = require('express');
const router = express.Router();
const Messages = require('../models/chat');


router.post('/send', (req, res) => {
    console.log(req.body);
    try {
        const message = new Messages({
            message: req.body.message,
            senderId: req.body.senderId
        })
        message.save((err, messageData) => {
            res.status(200).send(messageData);
        })
    } catch (error) {
        res.status(400).send('some error there while sending a messages')
    }
})

router.get('/get', (req, res) => {
    Messages.find((err, msgData) => {
        if (err) {
            res.send(err);
        }
        res.status(200).send(msgData);
    })
})

module.exports = router;